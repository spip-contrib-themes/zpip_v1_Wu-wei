<?php
if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
    // T
    'theme_wuwei_description' => 'A clean, minimalist, grid-based theme, with high contrast and multiple colour schemes.',
    'theme_wuwei_slogan' => 'White background, high contrast Z template',
    'theme_wuwei_titre' => 'Wu Wei white',
);