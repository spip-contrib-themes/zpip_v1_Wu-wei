<?php
if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
    // T
    'theme_wuwei_description' => 'Un thème sobre et minimaliste, basé sur une grille, avec un fort contraste.',
    'theme_wuwei_slogan' => 'Thème Z fond blanc, fort contraste',
    'theme_wuwei_titre' => 'Wu Wei blanc',
);